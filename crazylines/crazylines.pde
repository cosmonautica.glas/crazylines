// Import the library
import garciadelcastillo.dashedlines.*;

// Declare the main DashedLines object
DashedLines dash;
DashedLines dash2;
float dist = 0;
float dist2 = 0;
float linedistance;
int[] numbers = new int[100];


void setup() {
  background(10);

  size(800, 800);
  // Initialize it, passing a reference to the current PApplet
  dash = new DashedLines(this);
  dash2 = new DashedLines(this);
  int sero = 0;
  for (int i=0; i<100; i++) {
    numbers[i] = int(random(10));
    if (i<10) {
      numbers[i] = sero;
    }
  }




  // Set the dash-gap pattern in pixels
  dash.pattern(10, 5);
}

void draw() {
  linedistance =30;

  background(10);
  //show_number_of_frames();
  numbermatrix_even();
  change_height_and_print_lines();
  draw_outfading_lines();
  draw_outfading_lines_mooving();
  draw_outfading_lines_mooving2();
}

void change_height_and_print_lines(){
  for (int i=0; i<25; i++) {
    if (i == 0 ||i==2||i == 3||i == 5 ||i == 7 ||i == 11 ||i == 17 || i == 19) {
      int line_height = int(random(-500, frameCount%600));
      numbermatrix_odd(i, line_height);
    }
  }
}

void show_number_of_frames() {
  pushMatrix();
  fill(255, 255, 255);
  textSize(32);
  text(frameCount, 10, 30);
  popMatrix();
}
void numbermatrix_even() {
  pushMatrix();
  fill(110, 255, 10, 37);
  textSize(21);
  for (int l = 0; l < 30; l++) {
    for (int i = 0; i<100; i++) {
      if (l%2 ==0) {
        text(numbers[(i+l)%99], l*80+20, (-250+i*20+frameCount*2*noise(i*0.03))%2000);
      }
      if (l%2 ==1) {
        //fill(0, 127, 255, 50);
        text(numbers[(i+15)%100], l*80-20, ((height+50-l*0.05-frameCount*3*noise(i*0.1)%2000)));
      }
    }
  }
  popMatrix();
}
void numbermatrix_odd(int rownumber, int height_line) {
  pushMatrix();
  fill(110, 255, 10, 3);
  textSize(21);
  for (int l = 0; l < 20; l++) {
    for (int i = 0; i<20; i++) {
  //    text(numbers[int(99*noise(i*3))], rownumber*40, ((height*(2/3)+l*25-20*noise(frameCount*0.0002))%2000)+height_line*0.5);
      text(numbers[l], rownumber*40, ((height*(2/3)+l*25-20*noise(frameCount*0.0002))%2000)+height_line*0.5);
    }
  }
  popMatrix();
}
void draw_outfading_lines() {
  pushMatrix();
  noFill();
  int color_gradient = 15;
  for (float i = 0; i<15; i= i+1) {
    beginShape();
    stroke(220 - i*color_gradient);
    vertex(0, 20+i*linedistance);
    vertex(200, 20+i*linedistance);
    vertex(300, 80+i*linedistance);
    vertex(550, 80+i*linedistance);
    vertex(700, 20+i*linedistance);
    vertex(800, 20+i*linedistance);
    endShape();
  }
  popMatrix();
}

void draw_outfading_lines_mooving() {
  int counter = 0;
  pushMatrix();
  linedistance = linedistance +10;

  int color_gradient = 5;
  dash. pattern(800, 1000);
  //  dash2.pattern(200,3,5,3);
  for (int i = 1; i<10; i= i+3) {
    noFill();
    dash.beginShape();
    stroke(220 - i*color_gradient);
    dash.vertex(0, 20+i*linedistance);
    dash.vertex(200, 20+i*linedistance);
    dash.vertex(300, 80+i*linedistance);
    dash.vertex(550, 80+i*linedistance);
    dash.vertex(700, 20+i*linedistance);
    dash.vertex(800, 20+i*linedistance);
    dash.endShape();
    counter++;
  }
  dash.offset(dist);
  dist += 3;
  popMatrix();
}

void draw_outfading_lines_mooving2() {
  pushMatrix();
  linedistance = 500;

  int color_gradient = 5;
  dash2. pattern(800, 1000);
  //  dash2.pattern(200,3,5,3);
  noFill();
  dash2.beginShape();
  stroke(220 - color_gradient);
  dash2.vertex(0, 20+linedistance);
  dash2.vertex(200, 20+linedistance);
  dash2.vertex(300, 80+linedistance);
  dash2.vertex(550, 80+linedistance);
  dash2.vertex(700, 20+linedistance);
  dash2.vertex(800, 20+linedistance);
  dash2.endShape();

  dash2.offset(dist);
  dist2 += 6;
  popMatrix();
}
